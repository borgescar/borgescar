<?php

class Cliente extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('ClienteDAO', 'mCliente');
    }

    function cadastrarCliente() {

        if ($this->post("nome") != null ||
            $this->post("email") != null ||
            $this->post("telefone") != null) {

            $dados = [
                'nome' => $this->post("nome"),
                'telefone' => $this->post("telefone"),
                'email' => $this->post("email")
            ];


            if ($this->m['mCliente']->inserirCliente($dados) == true) {
                redirect("home", "Cadastro realizado com sucesso!");
            } else {
                redirect("home", "Erro ao salvar os dados!");
            }
        } else {
            redirect("home", "Informe todos os campos!");
        }
    }
}
