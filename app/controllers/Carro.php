<?php

class Carro extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('Carro_model', 'carro');
        $this->loadModel('Usuario_model', 'usuario');
        $this->loadModel('Upload_model', 'upload');
        $this->loadModel('CategoriaDAO', 'mCategoria');

        session_start();
        if ($_SESSION['logado'] != 'logado') {
            header('Location: http://localhost/borgescar/login');
        }
    }

    function inserirCarro() {
        $imagem = $this->m['upload']->uploadImg();
        
        $dados = [
            'marca' => $this->post('marca'),
            'modelo' => $this->post('modelo'),
            'ano_fabricacao' => $this->post('anoFabricacao'),
            'cor' => $this->post('cor'),
            'id_combustivel' => $this->post('tipoCombustivel'),
            'valor' => str_replace("R$ ", "", str_replace(",", ".", str_replace(".", "", $this->post('valor')))),
            'id_usuario' => $this->m['usuario']->retornarIdUsuario(),
            'fl_vendido' => 0,
            'id_categoria' => $this->post('idcategoria'),
            'imagem' => $imagem,
        ];
        
        
        if ($imagem != "erro") {
            $inserirCarro = $this->m['carro']->inserirCarro($dados);

            if ($inserirCarro == true) {   
                redirect('admin/abaListarCarros', 'Carro inserido com sucesso!');
            } else {
                redirect('admin/abaCadastrarCarros', 'Erro ao inserir carro, tente novamente!');
            }
        }
    }

    function editarCarro($idCarro) {
        
        $dados = [
            'marca' => $this->post('marca'),
            'modelo' => $this->post('modelo'),
            'ano_fabricacao' => $this->post('anoFabricacao'),
            'cor' => $this->post('cor'),
            'id_combustivel' => $this->post('tipoCombustivel'),
            'valor' => str_replace("R$ ", "", str_replace(",", ".", str_replace(".", "", $this->post('valor')))),
            'fl_vendido' => $this->post('fl_vendido'),
            'id_categoria' => $this->post('idcategoria')
        ];

        $editarCarro = $this->m['carro']->editarCarro($idCarro, $dados);
        if ($editarCarro == true) {
            redirect('admin/abaListarCarros', 'Carro editado com sucesso!');
        } else {
            redirect('admin/abaCadastrarCarros', 'Erro ao editar carro, tente novamente!');
        }
    }

    function excluirCarro($idCarro, $img) {
        $url = $_SERVER['DOCUMENT_ROOT'];
        $pastaDel = $url . '/borgescar/assets/img/';
        $imgDecode = base64_decode($img);
        unlink("$pastaDel$imgDecode");
        
        if($this->m['carro']->excluirCarro(base64_decode($idCarro))){
            redirect('admin/abaListarCarros', 'Carro excluído com sucesso!');
        }else{
            redirect('admin/abaListarCarros', 'Erro ao excluir carro!');
        }
    }
    
    
    
    

}
