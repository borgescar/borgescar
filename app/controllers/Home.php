<?php

/**
 * Description of Home
 *
 * @author Helder dos Santos
 */
class Home extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('Carro_model', 'carro');
        $this->loadModel('ClienteDAO', 'mCliente');
        $this->loadModel('CategoriaDAO', 'mCategoria');
        $this->loadModel('PropostaDAO', 'mProposta');
        $this->loadModel('UsuarioDAO', 'mUsuario');
        $this->loadModel('Upload_model', 'upload');
    }

    public function index() {
        $resultado['categorias'] = $this->m['mCategoria']->consultaCategoria();
        $resultado['carro'] = $this->m['carro']->consultaCarroNaoVendido();
        $resultado['categoria'] = $this->m['mCategoria']->consultaCategoria();
        $resultado['marcas'] = $this->m['carro']->buscaMarcas();

        $this->loadView('header');
        $this->loadView('conteudo-inicio', $resultado);
        $this->loadView('footer');
    }

    function buscarCarros() {
        $dados = [
            'marca' => $this->post('marca'),
            'modelo' => $this->post('modelo'),
            'id_categoria' => $this->post('categoria'),
            'valorDe' => str_replace("R$ ", "", str_replace(",", ".", str_replace(".", "", $this->post('valor-de')))),
            'valorAte' => str_replace("R$ ", "", str_replace(",", ".", str_replace(".", "", $this->post('valor-ate')))),
            'anoDe' => $this->post('data-de'),
            'anoAte' => $this->post('data-ate'),
        ];
        
        $resultado['carro'] = $this->m['carro']->buscaParametrizada($dados);
        
        $this->loadView('header');
        $this->loadView("buscaCarros", $resultado);
        $this->loadView('footer');   
    }

    function buscarModelos() {
        $marcas = $this->post("marc");
        $marcas = json_encode($this->m['carro']->buscaModelos($marcas));
        echo $marcas;
    }
}
