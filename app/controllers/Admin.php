<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('Carro_model', 'carro');
        $this->loadModel('Login_model', 'login');
        $this->loadModel('PropostaDAO', 'proposta');
        $this->loadModel('Usuario_model', 'usuario');
        session_start();
        if ($_SESSION['logado'] != 'logado') {
            header('Location: http://localhost/borgescar/login');
        }
    }

    function index() {
        $dados['propostas'] = $this->m['proposta']->consultaProposta();

        $this->loadView('admin/header');
        
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        
        $this->loadView('admin/pagina-admin-master', $dados);
        $this->loadView('admin/footer');
    }

    function abaListarCarros() {
        $resultado['listarCarros'] = $this->m['carro']->retornarCarros();
        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/lista-carros', $resultado);
        $this->loadView('admin/footer');
    }

    function abaCadastrarCarros() {
        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/cadastro-carro');
        $this->loadView('admin/footer');
    }

    function abaEditarCarros($idCarro) {
        $resultado['carroPorId'] = $this->m['carro']->retornarCarroPorId($idCarro);
        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/editar-carro', $resultado);
        $this->loadView('admin/footer');
    }

    function abaListarUsuarios() {
        $this->verificarNivelAdm();
        
        $resultado['listarUsuarios'] = $this->m['usuario']->retornarUsuarios();
        $this->loadView('admin/header');
        $this->loadView('admin/menu-admin-master');
        $this->loadView('admin/lista-usuarios', $resultado);
        $this->loadView('admin/footer');
    }

    function abaCadastrarUsuarios() {
        $this->verificarNivelAdm();
        
        $this->loadView('admin/header');
        $this->loadView('admin/menu-admin-master');
        $this->loadView('admin/cadastro-usuario');
        $this->loadView('admin/footer');
    }

    function abaEditarUsuarios($idUsuario) {
        $this->verificarNivelAdm();

        $resultado['usuarioPorId'] = $this->m['usuario']->retornarUsuarioPorId($idUsuario);
        $this->loadView('admin/header');
        $this->loadView('admin/menu-admin-master');
        $this->loadView('admin/editar-usuario', $resultado);
        $this->loadView('admin/footer');
    }

    function abaMinhaConta() {
        $idUsuario = $this->m['usuario']->retornarIdUsuario();
        $resultado['usuarioPorId'] = $this->m['usuario']->retornarUsuarioPorId($idUsuario);

        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/minha-conta', $resultado);
        $this->loadView('admin/footer');
    }

    function abaListarPropostas() {
        $resultado['listarPropostas'] = $this->m['proposta']->consultaProposta();

        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/lista-propostas', $resultado);
        $this->loadView('admin/footer');
    }

    function abaResponderProposta($idProposta) {
        $resultado['dadosProposta'] = $this->m['proposta']->consultaPropostaPorId($idProposta);
        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/responder-proposta', $resultado);
        $this->loadView('admin/footer');
    }

    function abaEmailPromocional() {
        $this->loadView('admin/header');
        if ($_SESSION['admin'] == 'sim') {
            $this->loadView('admin/menu-admin-master');
        } else if ($_SESSION['admin'] == 'nao') {
            $this->loadView('admin/menu-admin');
        }
        $this->loadView('admin/email-promocional');
        $this->loadView('admin/footer');
    }
    
    private function verificarNivelAdm(){
        if ($_SESSION['admin'] != 'sim') {
            header('Location: http://localhost/borgescar/login');
        }
    }

}
