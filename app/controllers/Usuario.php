<?php

class Usuario extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('Usuario_model', 'usuario');

        session_start();
        if ($_SESSION['logado'] != 'logado') {
            header('Location: http://localhost/borgescar/login');
        }
    }

    /**
     * Função que verifica se o nome de usuário já existe no sistema.
     * @param type $login
     * @return boolean
     */
    private function verificaUsuario($login) {
        if ($this->m['usuario']->verificarUsuario($login) === $login) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Função que verifica se o número de cpf já existe no sistema.
     * @param type $cpf
     * @return boolean
     */
    private function verificaCpf($cpf) {
        if ($this->m['usuario']->verificarCpf($cpf) === $cpf) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /**
     * Permite aos administradores cadastrar novos usuários no sistema
     */
    function inserirUsuario() {

        if ($_SESSION['admin'] != 'sim') {
            header('Location: http://localhost/borgescar/login');
        }

        $nome = strtolower($this->post('nome'));
        $email = strtolower($this->post('email'));
        $cpf = preg_replace("/\D+/", "", $this->post('cpf'));

        $telefone = preg_replace("/\D+/", "", $this->post('telefone'));
        $cpf = preg_replace("/\D+/", "", $this->post('cpf'));

        if (validaCPF($cpf)) {
            if ($this->verificaCpf($cpf) == false) {
                echo "<script type='text/javascript'>alert('CPF já cadastrado no sistema!');</script>";
                echo "<script>javascript:history.back(-2)</script>";
            } else {

                $login = $this->post('usuario');

                if ($this->verificaUsuario($this->post('usuario')) == false) {
                    echo "<script type='text/javascript'>alert('Usuário já cadastrado no sistema!');</script>";
                    echo "<script>javascript:history.back(-2)</script>";
                } else {
                    $encrypt_md = md5($this->post('senha'));
                    $encrypt_base = base64_encode($encrypt_md);
                    $senha = $encrypt_base;
                    $fl_ativo = 1;
                    $fl_masculino = $this->post('sexo');
                    $fl_admin = 0;
                    $inserirUsuario = $this->m['usuario']->inserirUsuario($nome, $email, $telefone, $cpf, $login, $senha, $fl_ativo, $fl_masculino, $fl_admin);

                    if ($inserirUsuario == true) {
                        redirect('admin/abaListarUsuarios', 'Usuário cadastrado com sucesso!');
                    } else {
                        echo "<script type='text/javascript'>alert('Erro ao inserir usuário, tente novamente!');</script>";
                        echo "<script>javascript:history.back(-2)</script>";
                    }
                }
            }
        } else {
            echo "<script type='text/javascript'>alert('O CPF informado é inválido!');</script>";
            echo "<script>javascript:history.back(-2)</script>";
        }
    }

    /**
     * Permite aos usuários administradores editar os dados dos clientes cadastrados no sistema
     * @param type $idUsuario
     */
    function editarUsuario($idUsuario) {

        if ($_SESSION['admin'] != 'sim') {
            header('Location: http://localhost/borgescar/login');
        }

        $nome = strtolower($this->post('nome'));
        $fl_masculino = $this->post('sexo');
        $email = strtolower($this->post('email'));
        $telefone = preg_replace("/\D+/", "", $this->post('telefone'));
        $fl_ativo = $this->post('fl_ativo');

        $editarUsuario = $this->m['usuario']->editarUsuario($idUsuario, $nome, $fl_masculino, $email, $telefone, $fl_ativo);
        if ($editarUsuario == true) {

            redirect('admin/abaListarUsuarios', 'Usuário editado com sucesso!');
        } else {
            echo "<script type='text/javascript'>alert('Erro ao editar usuário, tente novamente!');</script>";
            echo "<script>javascript:history.back(-2)</script>";
        }
    }

    /**
     * Responsável por permitir aos usuários a alteração de suas senhas
     */
    function alterarSenha() {
        $idUsuario = $this->m['usuario']->retornarIdUsuario();
        $encrypt_md = md5($this->post('senhaAtual'));
        $encrypt_base = base64_encode($encrypt_md);
        $senhaAtual = $encrypt_base;

        $encrypt_md2 = md5($this->post('novaSenha'));
        $encrypt_base2 = base64_encode($encrypt_md2);
        $novaSenha = $encrypt_base2;

        $retorno = $this->m['usuario']->verificarSenhaUsuario($senhaAtual);
        if ($retorno == null) {
            echo "<script type='text/javascript'>alert('Senha atual incompatível com o que foi digitado!');</script>";
            echo "<script>javascript:history.back(-2)</script>";
        } else {
            $this->m['usuario']->inserirNovaSenha($novaSenha, $idUsuario);
            redirect('admin/abaMinhaConta', 'Senha alterada com sucesso!');
        }
    }

}
