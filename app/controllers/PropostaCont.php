<?php

class PropostaCont extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('propostaDAO', 'mProposta');
    }

    public function inserirProposta() {
        $data = date("Y-m-d");
        $resposta;
        if ($this->post("nome") != null ||
                $this->post("email") != null ||
                $this->post("telefone") != null ||
                $this->post("proposta") != null ||
                $this->post("carro") != null) {

            $dados = [
                'nome_pessoa' => $this->post("nome"),
                'email' => $this->post("email"),
                'telefone' => $this->post("telefone"),
                'proposta' => $this->post("proposta"),
                'id_carro' => $this->post("carro"),
                'fl_analisado' => 0,
                'analise' => null,
                'data' => $data,
            ];

            if ($this->m['mProposta']->inserirProposta($dados) == true) {
                $resposta = array("msg" => "Proposta salva com sucesso!");
            }else{
                $resposta = array("msg" => "Erro ao salvar os dados!");
            }
            
        } else {
            $resposta = array("msg" => "Não foi possível salvar a proposta");
        }

        echo json_encode($resposta);
    }
    
    
    /**
     * reponsável por registrar a resposta de 
     * uma proposta informada pelo cliente e 
     * enviar o e-mail co a resposta
     */
    public function inserirResposta() {
        
        $idProposta = strtolower($this->post('id-proposta'));
        $resposta = strtolower($this->post('resposta'));
        $fl_analisado = 1;
        $nome = strtolower($this->post('nome-proposta'));
        $email = strtolower($this->post('email-proposta'));
        
        $dados = [
            'analise' => $resposta,
            'fl_analisado' => $fl_analisado,
        ];
        
        $inserirResposta = $this->m['mProposta']->inserirResposta($idProposta, $dados);
        
        if ($inserirResposta == true) {
            
            if(enviar_email($nome, $email, $resposta, 'Sobre sua proposta de compra de veículo')){
                redirect('admin/abaListarPropostas', 'Resposta inserida com sucesso!');
            }else{
                redirect('admin/abaListarPropostas', 'Erro ao enviar o e-mail!');
            }
            
        } else {
            echo "<script type='text/javascript'>alert('Erro ao inserir resposta, tente novamente!');</script>";
            echo "<script>javascript:history.back(-2)</script>";
        }
    }

}
