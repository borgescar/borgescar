<?php

/**
 * classe responsável pelo envio de e-mail ao clientes cadastrados
 * no sistema
 * @author Helder dos Santos Sousa
 */
class EnvioEmail extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel("ClienteDAO", "mCliente");

        session_start();
        if ($_SESSION['logado'] != 'logado') {
            header('Location: http://localhost/borgescar/login');
        }
    }

    public function enviarEmailPromocional() {
        $mensagem = $this->post("email-promocional");
        $assunto = $this->post("assunto");
        $statusEnvio;
        
        $clientes = $this->m['mCliente']->consultaCliente();
        
        foreach ($clientes as $c){
            $statusEnvio = enviar_email($c['nome'], $c['email'], $mensagem, $assunto);
        }
        
        if($statusEnvio === true){
            $statusEnvio = "E-mail enviado com sucesso a todos os clientes!";
        }else{
            $statusEnvio = "Ocorreu um erro ao enviar o e-mail!!";
        }
        
        redirect("admin/abaemailpromocional", $statusEnvio);
    }

}
