<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author Helder dos Santos
 */
class Login extends Controller {

    public function __construct() {
        parent::__construct();
        $this->loadModel('Login_model', 'login');
        $this->loadModel('UsuarioDAO', 'usuario');
    }

    public function index() {
        session_start();
        if (isset($_SESSION['logado'])) {
            header('Location: http://localhost/borgescar/admin/index');
        } else {
            $this->loadView('header-login');
            $this->loadView('login');
        }
    }

    public function logar() {
        $usuario = $this->post('usuario');
        $encrypt_md = md5($this->post('senha'));
        $encrypt_base = base64_encode($encrypt_md);
        $senha = $encrypt_base;
        $result = $this->m['login']->consultaUsuarioSenha($usuario, $senha);
        foreach ($result as $res) {
            $logUser = $res['login'];
            $flAtivo = $res['fl_ativo'];
            $flAdmin = $res['fl_admin'];
        }
        if ($result == null) {
            redirect('login', 'Usuario ou senha invalido(s)!');
        } else {
            if ($flAtivo == '0') {
                redirect('login', 'Usuario nao esta ativo no sistema!');
            } else {
                if ($logUser == $usuario) {
                    session_start();
                    $_SESSION['logado'] = 'logado';
                    $_SESSION['usuario'] = $logUser;
                    if ($flAdmin == '1') {
                        $_SESSION['admin'] = 'sim';
                        redirect('Admin/index');
                    } else {
                        $_SESSION['admin'] = 'nao';
                        redirect('admin/index');
                    }
                }
            }
        }
    }

    public function sair() {
        session_start();
        session_destroy();
        redirect("login");
    }

    public function abaRecuperarSenha() {
        $this->loadView('recuperarSenha');
    }

    public function recuperarSenha() {
        $email = $this->post('emailRec');
        $login = $this->post('usuarioRec');
        $result = $this->m['login']->verificaEmailLogin($email, $login);
        $nomeUsuario;

        if ($result == null) {
            echo "<script type='text/javascript'>alert('E-mail e/ou Usuário são inválido(s) ou não coincidem, tente novamente!');</script>";
            echo "<script>javascript:history.back(-2)</script>";
        } else {
            foreach ($result as $res) {
                $id = $res['id_usuario'];
                $nomeUsuario = $res['nome_usuario'];
            }

            $string_ns = substr(md5(time()), 0, 10);

            if (enviar_email(
                            $nomeUsuario, 
                            $email, 
                            'Nova senha para acesso do sistema Borges Car ' . '<br>Login: <b>' . $login . '</b><br>' . 'Senha: <b>' . $string_ns.'</b>', 
                            'Nova senha para acesso do sistema Borges Car'
                            )) {
                $encrypt_md = md5($string_ns);
                $encrypt_base = base64_encode($encrypt_md);
                $this->m['login']->inserirNovaSenha($encrypt_base, $id);

                redirect('login', 'Sua nova senha foi enviada por E-mail!');

            } else {
                redirect('login/abaRecuperarSenha', 'Erro ao enviar formulário: ' . print($mail->ErrorInfo));
            }
            
            
        }
    }

}
