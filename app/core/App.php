<?php

include_once 'app/config/config.php'; 
/**
 * Faz a chamada das classes
 * bem como seu respectivos métodos e parametros
 * esta classe está sendo referenciada no
 * arquivo index.php do diretório raiz
 *
 * @author Helder dos Santos
 */
class App {

    //armazena o controller que deve ser chamdo
    protected $controller = default_controller;
    //amrzena o methodo que deve ser chamado
    protected $method = "index";
    //armazena os parametros que serão passados aos métodos
    protected $param = [];
    
    public $base_url = "";
    
    private static $instance;

    private function __construct() {
        $this->carregamentoInicial();
        
        if (isset($_GET['url'])) {
            $url = self::parseUrl($_GET['url']);

            //verifica se o controller especificado existe
            if (file_exists('app/controllers/' . $url[0] . '.php')) {
                $this->controller = $url[0];
                unset($url[0]);
                require_once 'app/controllers/' . $this->controller . '.php';
                $this->controller = new $this->controller;
            } else {
                $this->loadPagErro();
            }



            //verifica se o metodo existe
            if (isset($url[1])) {
                if (method_exists($this->controller, $url[1])) {
                    $this->method = $url[1];
                    unset($url[1]);
                } else {
                    $this->loadPagErro();
                }
            }

            //verifica se a variável URL ainda tem algum valor
            //então ele chama a função array_values de $url que passa todos os valore de url para $param
            //caso ontrário, continua sendo um array vazio        
            $this->param = $url ? array_values($url) : [];
        } else {
            require_once 'app/controllers/' . $this->controller . '.php';
            $this->controller = new $this->controller;
        }

        call_user_func_array([$this->controller, $this->method], $this->param);
    }
    
    public static function getInstance(){
        if(!isset(App::$instance))
            App::$instance = new App();
        
        return App::$instance;
    }

    /**
     * retorna um array com as palavras
     * correspodentes ao controller chamado, método e atributos
     * @param type $url
     * @return type
     */
    private function parseUrl($url) {
        //filter_var remove caracteres não permitidos na URL
        return explode("/", filter_var(rtrim($url)));
    }

    /**
     * carrega a página de erro 404
     */
    private function loadPagErro() {
        require_once 'app/controllers/paginaerro.php';
        $erro = new PaginaErro();
        $erro->index();
        exit();
    }
    
    /**
     * faz o carregamento inicial de arquivos necessário em toda a aplicação
     */
    private function carregamentoInicial(){
        include_once 'Controller.php';
        include_once 'Model.php';
        include 'app/database/Database.php';
        include 'app/config/util.php';
    }

}
