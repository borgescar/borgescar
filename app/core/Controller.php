<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controller
 *
 * @author Helder dos Santos
 */
class Controller {

    protected $m = []; //grava um array de model ao inicial um classe que herda de Controller


    public function __construct() {
        date_default_timezone_set('America/Sao_Paulo');
    }

    /**
     * Faz o caregamento de um view passando para ela os dados
     * de um array
     * 
     * @param type $view
     * @param type $dados
     */
    protected function loadView($view, $dados = []) {  
        if (file_exists('app/views/' . $view . '.php')) {
            require_once 'app/views/' . $view . '.php';
        }
        
    }

    /**
     * Faz o carregamento de uma model de acordo com o nome da model e retorna
     * a instancia criada
     * @param type $model
     * @param type $nome
     */
    protected function loadModel($model,$name) {
        if (file_exists('app/models/' . $model . '.php')) {
            require_once 'app/models/' . $model . '.php';
        }
        
        $this->m[$name] = new $model();
    }
    
    
    /**
     * Pega os dados que são passados via GET
     * e retorna-o, se não for entontrado, e retornado null
     * @param type $name
     * @return type
     */
    protected function get($name){
        if(isset($_GET[$name]))
            return addslashes($_GET[$name]);
        return null;
    }
    
    /**
     * Pega os dados que são passados via POST
     * e retorna-o, se não for entontrado, e retornado null
     * @param type $name
     * @return type
     */
    protected function post($name){
        if(isset($_POST[$name]))
            return addslashes($_POST[$name]);
        return null;
    }
    
    

}
