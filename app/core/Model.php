<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultModel
 *
 * @author Helder dos Santos
 */
class Model {

    private $conn;

    public function __construct() {
        $this->conn = Database::getIntance()->getConexao();
    }

    /**
     * Faz uma seleção dos dados de uma determinada tabela do banco de dados
     * EX: selectQuery("carro", "idcarro = $idCarro", "10");
     * @param type $tabela
     * @param type $where
     * @param type $limit
     * @return type
     */
    public function selectQuery($tabela, $where = NULL, $limit = NULL) {
        if ($where == null and $limit == null) {
            $SQL = $SQL = "SELECT * FROM ({$tabela})";
        } else if ($where != null && $limit == NULL) {
            $SQL = "SELECT * FROM ({$tabela}) WHERE ({$where})";
        } else if ($limit != null && $where == NULL) {
            $SQL = "SELECT * FROM ({$tabela}) LIMIT ({$limit})";
        } else {
            $SQL = "SELECT * FROM ({$tabela}) WHERE ({$where})LIMIT $limit";
        }

        $consulta = $this->conn->query($SQL);
        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }
    
     public function selectQueryDistinct($tabela, $where = NULL, $limit = NULL) {
        if ($where == null and $limit == null) {
            $SQL = $SQL = "SELECT DISTINCT * FROM ({$tabela})";
        } else if ($where != null && $limit == NULL) {
            $SQL = "SELECT DISTINCT * FROM ({$tabela}) WHERE ({$where})";
        } else if ($limit != null && $where == NULL) {
            $SQL = "SELECT DISTINCT * FROM ({$tabela}) LIMIT ({$limit})";
        } else {
            $SQL = "SELECT DISTINCT * FROM ({$tabela}) WHERE ({$where})LIMIT $limit";
        }

        $consulta = $this->conn->query($SQL);
        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function executarQuery($sql) {
        $inserir = $this->conn->prepare($sql);
        return $inserir->execute();
    }

    public function selectQueryInnerJoin($Tabela1, $Tabela2, $ChavePrimaria, $ChaveSecundaria, $TabelaWhere = null, $CampoWhere = null, $TermoWhere = null) {

        if ($TabelaWhere != null && $CampoWhere != null && $TermoWhere != null) {
            $SQL = "SELECT * FROM {$Tabela1} INNER JOIN {$Tabela2} ON {$Tabela1}.{$ChavePrimaria} = {$Tabela2}.{$ChaveSecundaria} AND {$TabelaWhere}.{$CampoWhere} = {$TermoWhere}";
        } else {
            $SQL = "SELECT * FROM {$Tabela1} INNER JOIN {$Tabela2} ON {$Tabela1}.{$ChavePrimaria} = {$Tabela2}.{$ChaveSecundaria}";
        }
        echo $SQL;
        $consulta = $this->conn->query($SQL);
        return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insertQuery($Tabela, array $Campos, array $Dados) {

        $CamposTabela = implode(" ", $Campos);
        $DadosInserir = implode("', '", $Dados);


        $SQL = "INSERT INTO $Tabela($CamposTabela) VALUES ('$DadosInserir')";
        $inserir = $this->conn->prepare($SQL);
        return $inserir->execute();
    }

    public function deleteQuery($Tabela, $coluna, $param) {
        $SQL = "DELETE FROM $Tabela WHERE $coluna = $param";
        $delete = $this->conn->prepare($SQL);
        return $delete->execute();
    }

    public function updateQuery($tabela, array $dados, $id, $param) {
        foreach ($dados as $key => $value) {
            $places[] = $key . ' = :' . "$key";
        }
        $places = implode(', ', $places);
        $sql = "UPDATE $tabela SET $places WHERE $param = :$param";
        $update = $this->conn->prepare($sql);
        $update->bindParam(':'.$param, $id);
        foreach ($dados as $key => &$val) {
            $update->bindParam($key, $val);
        }
        return $update->execute();
    }
    
    public function getConn(){
        return $this->conn;
    }

}
