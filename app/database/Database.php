<?php

/**
 * reponsável pelas ações de comunicação com o banco de dados.
 * 
 * Ela implementa o padrão de projeto SINGLETON para que ele só seja instanciada uma 
 * única vez.
 *
 * @author Helder dos Santos
 */
class Database{

    private $hostName = host;
    private $dbPorta = porta;
    private $userName = user_db;
    private $password = senha;
    private $nameBanco = db_name;
    private $conexao;
    private static $instance;

    private function __construct() {
        
    }

    /**
     * retorna a instancia da classe que é criada somente uma única vez
     * isso devido a implementação do padrão de projeto SINGLETON
     * @return Database
     */
    public static function getIntance() {
        if (!isset(Database::$instance)) {
            Database::$instance = new Database();
        }
        return Database::$instance;
    }

    /**
     * Retorna uma conexao criada, onde há uma verificação se a conexao já foi criada,
     * se não, a função cria a conexão e a retorna.
     * @return PDO
     */
    public function getConexao() {
        if (!isset($this->conexao)) {
            try {
                $this->conexao = new PDO('mysql:host=' . $this->hostName .
                        ';dbname=' . $this->nameBanco .
                        ";port=" . $this->dbPorta, $this->userName, $this->password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            } catch (PDOException $exc) {
                echo "Erro ao conectar com base de dados: " . $exc->getMessage();
            }
        }
        return $this->conexao;
    }

}
