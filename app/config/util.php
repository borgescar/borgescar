<?php

include_once 'app/config/config.php';

/**
 * Redireciona para um controller
 * @param type $url
 * @param type $alerta
 */
function redirect($url, $alerta = null) {


    if ($alerta != null) {
        echo "<script>alert('" . $alerta . "');</script>";
    }

    echo "<script>"
    . "window.location.href = '"
    . base_url . $url
    . "';"
    . "</script>";
}

function base_url($url = null) {
    if ($url != null) {
        return base_url . $url;
    } else {
        return base_url;
    }
}

/**
 * Faz a configuração para o envio de e-mail
 * @param type $distinatario = nome de quem vai receber o e-mail
 * @param type $email = e-mail de quem vai receber o e-mail
 * @param type $mensagem = mensagem do -email
 * @param type $assunto = Assunto que vai aparecer ao chegar o e-mail
 * @return boolean = verdadeiro se for enviado, se não, falso
 */
function enviar_email($distinatario, $email, $mensagem, $assunto) {

    $enviaFormularioParaNome = $distinatario;
    $enviaFormularioParaEmail = $email;

    $caixaPostalServidorNome = 'Borges Car';
    $caixaPostalServidorEmail = 'contatothelmi@gmail.com';
    $caixaPostalServidorSenha = 'leaosampaio';

    $mensagemConcatenada = $mensagem;
    require_once('app/bibliotecas/email/PHPMailerAutoload.php');
    $mail = new PHPMailer();

    $mail->IsSMTP();
    $mail->SMTPAuth = true;
    $mail->Charset = 'utf8_decode()';
    $mail->Host = 'ssl://smtp.gmail.com';
    $mail->Port = '465';
    $mail->Username = $caixaPostalServidorEmail;
    $mail->Password = $caixaPostalServidorSenha;
    $mail->From = $caixaPostalServidorEmail;
    $mail->FromName = utf8_decode($caixaPostalServidorNome);
    $mail->IsHTML(true);
    $mail->Subject = utf8_decode($assunto);
    $mail->Body = utf8_decode($mensagemConcatenada);

    $mail->AddAddress($enviaFormularioParaEmail, utf8_decode($enviaFormularioParaNome));

    if ($mail->Send()) {
        return true;
    } else {
        return false;
    }
}

/**
 * Faz a validação de cpf
 * @param type $cpf = cpf formatado (somente número)
 * @return boolean
 */
function validaCPF($cpf) { 
    // Verifica se o número digitado contém todos os digitos
    $cpf = str_pad(ereg_replace('[^0-9]', '', $cpf), 11, '0', STR_PAD_LEFT);

    // Verifica se nenhuma das sequências abaixo foi digitada, caso seja, retorna falso
    if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
        return false;
    } else {   // Calcula os números para verificar se o CPF é verdadeiro
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }

            $d = ((10 * $d) % 11) % 10;

            if ($cpf{$c} != $d) {
                return false;
            }
        }

        return true;
    }
}
