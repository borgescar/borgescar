<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CategoriaDAO
 *
 * @author Erlanio
 */
class CategoriaDAO extends Model {

    public function consultaCategoria() {
        $categoria = $this->selectQuery("categoria");
        return $categoria;
    }

    public function inserirCategoria($categoria) {
        $Dados = [
            'idcategoria' => null,
            'categoria' => $categoria,
        ];

        $Campos = ["idcategoria, categoria"];

        $inserirCategoria = $this->insertQuery('categoria', $Campos, $Dados);
        return $inserirCategoria;
    }

    public function deleteCategoria() {
        $id = 2;
        $deleteCategoria = $this->deleteQuery('categoria', 'idcategoria', $id);
    }

}
