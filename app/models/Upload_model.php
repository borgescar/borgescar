<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Upload
 *
 * @author Erlanio
 */
class Upload_model extends Model {

    public function uploadImg() {
        if (isset($_POST['enviar'])) {
            $nome = $_FILES['arquivo']['name'];
            $formato = $_FILES['arquivo']['type'];
            $formatos = array("imagem/jpg", "image/jpeg", "imagem/png", "imagem/bmp");
            $testType = array_search($formato, $formatos);

            if (!$testType) {
                echo "<script type='text/javascript'>alert('O formato da imagem é inválido, tente novamente!');</script>";
                echo "<script>javascript:history.back(-2)</script>";
                return "erro";
            } else {
                if (file_exists("assets/img/$nome")) {
                    $a = 1;
                    while (file_exists("assets/img/[$a]$nome")) {
                        $a++;
                    }
                    $nome = "[" . $a . "]" . $nome;
                }

                if (move_uploaded_file($_FILES['arquivo']['tmp_name'], "assets/img/" . $nome)) {
                    return $nome;
                } else {
                    echo "<script type='text/javascript'>alert('Falha no envio da imagem, tente novamente!');</script>";
                    echo "<script>javascript:history.back(-2)</script>";
                    return "erro";
                }
            }
        } else {
            echo "<script type='text/javascript'>alert('falha no envio');</script>";
            echo "<script>javascript:history.back(-2)</script>";
            return "erro";
        }

       

    }
    

    
}
