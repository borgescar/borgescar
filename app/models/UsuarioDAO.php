<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of UsuarioDAO
 *
 * @author Erlanio
 */
class UsuarioDAO extends Model{
    public function consutaUsuario() {
        $usuario = $this->selectQuery("usuario");
        return $usuario;
    }

    public function inserirUsuario() {
        $Dados = [
            'nome_usuario' => 'Helder dos Santos Sousa',
            'email' => 'heldersantos764@gmail.com',
            'telefone' => '8896042686',
            'cpf' => '071712603303',
            'login' => 'helder',
            'senha' => base64_encode(md5("helder")),
            'fl_ativo' => '1',
            'fl_masculino' => '0',
            'fl_admin' => '0',
        ];
        
        $Campos = ["nome_usuario, email, telefone, cpf, login, senha, fl_ativo, fl_masculino, fl_admin"];

        $inserirUsuario = $this->insertQuery('usuario', $Campos, $Dados);
        return $inserirUsuario;
    }

    public function deleteUsuario($id) {     
        $deleteUsuario = $this->deleteQuery('usuario', 'idusuario', $id);
    }
}
