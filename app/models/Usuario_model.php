<?php

class Usuario_model extends Model {
 
    public function retornarIdUsuario() {
        $nomeUsuario = $_SESSION['usuario'];
        $consulta = $this->selectQuery("usuario", "login = '$nomeUsuario'");
        $result = null;
        foreach ($consulta as $cons):
            $result = $cons['id_usuario'];
        endforeach;
        return $result;
    }
    
    public function retornarUsuarios() {
        $usuarios = $this->selectQuery('usuario');
        return $usuarios;
    }
    
    public function retornarUsuarioPorId($idUsuario) {
        $usuario = $this->selectQuery('usuario', "id_usuario = '$idUsuario'");
        return $usuario;
    }
    
    public function inserirUsuario($nome, $email, $telefone, $cpf, $login, $senha, $fl_ativo, $fl_masculino, $fl_admin) {
        $Dados = [
            'nome_usuario' => $nome,
            'email' => $email,
            'telefone' => $telefone,
            'cpf' => $cpf,
            'login' => $login,
            'senha' => $senha,
            'fl_ativo' => $fl_ativo,
            'fl_masculino' => $fl_masculino,
            'fl_admin' => $fl_admin,
        ];

        $Campos = [
            "nome_usuario, email, telefone, cpf, login, senha, fl_ativo, fl_masculino, fl_admin"
        ];
        $this->insertQuery("usuario", $Campos, $Dados);
        return true;
    }
    
    public function editarUsuario($idUsuario, $nome, $fl_masculino, $email, $telefone, $fl_ativo) {
        $dados = [
            'nome_usuario' => $nome,
            'email' => $email,
            'telefone' => $telefone,
            'fl_ativo' => $fl_ativo,
            'fl_masculino' => $fl_masculino,
        ];

        return $this->updateQuery("usuario", $dados, $idUsuario, 'id_usuario');
    }
    
    //Verificar se o nome de usuario já existe na base de dados.
    public function verificarUsuario($usuario = null) {
        $sql = $this->selectQuery("usuario", "login = '$usuario'");
        $result = null;
        foreach ($sql as $user):
            $result = $user['login'];
        endforeach;
        return $result;
    }
    
    //Verificar se o CPF já existe na base de dados.
    public function verificarCpf($cpf = null) {
        $sql = $this->selectQuery("usuario", "cpf = '$cpf'");
        $result = null;
        foreach ($sql as $user):
            $result = $user['cpf'];
        endforeach;
        return $result;
    }
    
    //Verificar se a senha digitada é correta.
    public function verificarSenhaUsuario($senha) {
        $sql = $this->selectQuery("usuario", "senha = '$senha'");
        $result = null;
        foreach ($sql as $user):
            $result = $user['login'];
        endforeach;
        return $result; 
    }
    
    public function inserirNovaSenha($senha, $idUsuario) {
        $dados = [
            'senha' => $senha,
        ];

        return $this->updateQuery("usuario", $dados, $idUsuario, 'id_usuario');
    }
  
}

