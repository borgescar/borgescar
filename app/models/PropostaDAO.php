<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PropostaDAO
 *
 * @author Erlanio
 */
class PropostaDAO extends Model {

    public function consultaProposta() {
        $proposta = $this->selectQuery('proposta p, carro c, combustivel cb, categoria ct',
                'p.id_carro = c.id_carro AND c.id_categoria = ct.id_categoria AND c.id_combustivel = cb.id_combustivel');
        return $proposta;
    }

    public function consultaPropostaPorId($idProposta) {
        $proposta = $this->selectQuery("proposta", "id_proposta = '$idProposta'");
        return $proposta;
    }

    public function deleteProposta($id) {
        $deleteProposta = $this->deleteQuery('proposta', 'idproposta', $id);
    }

    public function inserirProposta($dados) {
        $campo = ["nome_pessoa, email, telefone, proposta, id_carro, fl_analisado, analise, data"];
        return $this->insertQuery('proposta', $campo, $dados);
    }

    public function inserirResposta($idProposta, $dados) {
        return $this->updateQuery("proposta", $dados, $idProposta, 'id_proposta');
    }

}
