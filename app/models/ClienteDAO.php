<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ClienteDAO
 *
 * @author Erlanio
 */
class ClienteDAO extends Model {

    public function consultaCliente() {
        $cliente = $this->selectQuery('cliente');
        return $cliente;
    }

    public function inserirCliente($dados) {
        $campos = ["nome, telefone, email"];

        $inserirCliente = $this->insertQuery("cliente", $campos, $dados);
        return $inserirCliente;
    }

    public function deleteCliente() {
        $id = 4;
        $deleteCliente = $this->deleteQuery('cliente', 'idcliente', $id);
        return $deleteCliente;
    }

}
