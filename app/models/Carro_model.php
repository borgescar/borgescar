<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CarroDAO
 *
 * @author Erlanio
 */
class Carro_model extends Model {

    public function consultaCarro() {
        $carros = $this->selectQuery('carro');
        return $carros;
    }

    public function consultaCarroNaoVendido() {
        $carros = $this->selectQuery('carro c, categoria ct, combustivel cb', 'c.id_categoria = ct.id_categoria AND c.id_combustivel = cb.id_combustivel AND c.fl_vendido = 0');
        return $carros;
    }

    public function retornarCarros() {
        $carros = $this->selectQueryDistinct('carro c, categoria ct, combustivel cb, usuario u', 'c.id_categoria = ct.id_categoria AND c.id_combustivel = cb.id_combustivel AND c.id_usuario = u.id_usuario');
        return $carros;
    }

    public function retornarCarroPorId($idCarro) {
        $carros = $this->selectQuery('carro c, categoria ct, combustivel cb, usuario u', "c.id_combustivel = cb.id_combustivel AND c.id_usuario = u.id_usuario AND c.id_categoria = ct.id_categoria AND c.id_carro = '$idCarro'");
        return $carros;
    }

    public function inserirCarro($dados) {
        $campos = [
            "marca, modelo, ano_fabricacao, cor, id_combustivel, valor, id_usuario, fl_vendido, id_categoria, imagem"
        ];
        $this->insertQuery("carro", $campos, $dados);
        return true;
    }

    public function editarCarro($idCarro, $dados) {
        return $this->updateQuery("carro", $dados, $idCarro, 'id_carro');
    }

    public function excluirCarro($idCarro) {
        return $this->deleteQuery('carro', 'id_carro', $idCarro);
    }

    public function buscaParametrizada($dados) {
        $where = " c.marca = '" . $dados['marca'] . "' AND c.modelo = '" . $dados['modelo'] . "' AND c.id_categoria = " . $dados['id_categoria']." AND ct.id_categoria = c.id_categoria AND cb.id_combustivel = c.id_combustivel";
        $between = " AND c.valor BETWEEN " . $dados['valorDe'] . " AND " . $dados['valorAte'] . " AND c.ano_fabricacao BETWEEN " . $dados['anoDe'] . " AND " . $dados['anoAte'];

        $busca = $this->getConn()->query("SELECT c.*, cb.*, ct.* FROM carro c, combustivel cb, categoria ct WHERE " . $where . $between);
        return $busca->fetchAll(PDO::FETCH_ASSOC);
    }

    public function buscaMarcas() {
        $busca = $this->getConn()->query("SELECT DISTINCT marca FROM carro");
        return $busca->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function buscaModelos($marca) {
        $busca = $this->getConn()->query("SELECT DISTINCT modelo FROM carro WHERE marca = '".$marca."'");
        return $busca->fetchAll(PDO::FETCH_ASSOC);
    }

}
