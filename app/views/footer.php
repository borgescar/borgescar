<footer class="section section-primary">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h1>Borges Car</h1>
                <h5>Copyright &copy; 2016 - Todos os Direitos Reservados</h5>
            </div>
            <div class="col-sm-6">
                <p class="text-info text-right">
                    <br>
                    <br>
                </p>
                
            </div>
        </div>
    </div>
</footer>
<div class="clear"></div>


<script type="text/javascript">
    baseUrl = "<?php echo base_url(); ?>";
    $(function () {
        $(".valor").maskMoney({prefix: 'R$ ',
            showSymbol: true, thousands: '.', decimal: ',', symbolStay: true});
    })

    $(".campo-ano").mask("9999");
    $(".telefone-mask").mask("(99) 9999-9999");
    $(".data-mask").mask("99/99/9999");
    $(".cpf-mask").mask("999.999.999-99");
</script>
</body></html>

