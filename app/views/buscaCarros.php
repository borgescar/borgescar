<div class="section" id="inicio-pagina">
    <div class="container">

        <div class="row">
            <div class="col-md-7">
                <div class="section">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if ($dados['carro'] != null) {
                                    echo "<h3 class='text-center'>Busca realizada para a marca " . $dados['carro'][0]["marca"] . "</h3>";
                                } else {
                                    echo "<h3 class='text-center'>:( Nenhum carro foi encontrado.</h3>";
                                }
                                ?>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="<?php echo base_url(); ?>" class="btn btn-primary">Voltar</a>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                            foreach ($dados['carro'] as $carro) {
                                echo "<div class='col-md-4 img-carros' style = 'margin-top:50px;'>";
                                echo "<a><img style = 'width: 360px; height: 225px' src='" . base_url . "assets/img/" . $carro['imagem'] . "' class='img-responsive'></a>"
                                . "<h3>" . $carro['marca'] . "</h3>"
                                . "<p>Marca: " . $carro['marca'] .
                                "<br>Modelo: " . $carro['modelo'] .
                                "<br>Ano: " . $carro['ano_fabricacao'] .
                                "<br>Valor: R$ " . number_format($carro['valor'], 2, ',', '.') .
                                "<br>Categoria: " . $carro['categoria'] .
                                "<br>Combustível: " . $carro['nome_combustivel'] . "</p>"
                                . "</a>"
                                . "<a onclick=\"alterarIdCarro(" . '\'' . $carro['id_carro'] . '\'' . ")\" class='btn btn-success' data-toggle='modal' data-target='#mdl-deixe-proposta'>Deixe sua proposta</a>";
                                echo "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Início popup deixar proposta-->

<div class="modal fade" id="mdl-deixe-proposta">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" contenteditable="true">Deixe sua proposta</h4>
            </div>

            <form  form-horizontal-role="form" id="mdl-deixe-proposta">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <input type="hidden" name="id-carro" id="id-carro">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputEmail3" class="control-label">Nome:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nome" placeholder="Nome completo">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label">Telefone:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="telefone" placeholder="Telefone">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label">E-mail:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" placeholder="E-mail">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label" contenteditable="true">Proposta:</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea rows="3" id="proposta" cols="10" class="form-control" placeholder="Qual a melhor maneira de você pagar por este carro?"></textarea>
                                </div>
                            </div>

                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button type = "reset" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Enviar Proposta</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Fim do popup deixar proposta-->
