<div class="section" id="inicio-pagina">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div id="fullcarousel-example" data-interval="6000" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">


                        <?php
                        $item = 1;
                        $tamanhoArray = count($dados['carro']);
                        $i = 0;

                        if ($tamanhoArray > 5) {
                            $i = $tamanhoArray - 5;
                        }

                        for ($i; $i < $tamanhoArray; $i++) {
                            if ($item == 1) {
                                echo "<div class='item active'>";
                            } else {
                                echo "<div class='item'>";
                            }

                            echo "<img style='height: 365px;' class='img-slide' src='" . base_url("assets/img/" . $dados['carro'][$i]['imagem']) . "'>";
                            echo "<div class = 'carousel-caption' " .
                            "style='text-shadow: black 0px 2px, black 2px 0px, black -2px 0px, " .
                            "black 0px -2px, black -1.4px -1.4px, black 1.4px 1.4px, " .
                            "black 1.4px -1.4px, black -1.4px 1.4px; ; font-weight: bold;'>";
                            echo "<h3>" . $dados['carro'][$i]['marca'] . " - " . $dados['carro'][$i]['modelo'] . "</h3>";
                            echo "<p>Ano: " . $dados['carro'][$i]['ano_fabricacao'] . " | Valor: R$: " . number_format($dados['carro'][$i]['valor'], 2, ',', '.') . " - Categoria: " . $dados['carro'][$i]['categoria'] . "<br>Combustível: " . $dados['carro'][$i]['nome_combustivel'] . "</p>";
                            echo "<a onclick=\"alterarIdCarro(" . '\'' . $dados['carro'][$i]['id_carro'] . '\'' . ")\"href='javascript:void(0)' class = 'btn btn-success' data-toggle='modal' data-target='#mdl-deixe-proposta'>Como você quer pagar?</a>";
                            echo "</div>";
                            echo "</div>";
                            $item++;
                        }

                        /* foreach ($dados['carro'] as $carro) {
                          if ($item == 1) {
                          echo "<div class='item active'>";
                          } else {
                          echo "<div class='item'>";
                          }

                          echo "<img style='height: 365px;' class='img-slide' src='" . base_url("assets/img/" . $carro['imagem']) . "'>";
                          echo "<div class = 'carousel-caption'>";
                          echo "<h3>" . $carro['marca'] ." - ". $carro['modelo']."</h3>";
                          echo "<p>Ano: " . $carro['ano_fabricacao'] . " | Valor: R$: " . number_format($carro['valor'], 2, ',', '.') . " - Categoria: " . $carro['categoria'] . "<br>Combustível: " . $carro['nome_combustivel'] . "</p>";
                          echo "<a onclick=\"alterarIdCarro(" . '\'' . $carro['id_carro'] . '\'' . ")\"href='javascript:void(0)' class = 'btn btn-success' data-toggle='modal' data-target='#mdl-deixe-proposta'>Como você quer pagar?</a>";
                          echo "</div>";
                          echo "</div>";
                          $item++;
                          } */
                        ?>
                    </div>
                    <a class="left carousel-control" href="#fullcarousel-example" data-slide="prev"><i class="icon-prev fa fa-angle-left"></i></a>
                    <a class="right carousel-control" href="#fullcarousel-example" data-slide="next"><i class="icon-next fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-5">
                <div class="panel panel-primary" id="pnl-endereco">
                    <div class="panel-heading">
                        <h2 class="panel-title">Onde Estamos?</h2>
                    </div>
                    <div class="panel-body">
                        <h5>Av. Padre Cícero, 0000, Triângulo, Juazeiro do Norte - CE</h5>
                    </div>
                    <div id="mapa">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3958.13676084267!2d-39.32952128580547!3d-7.225237794784094!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7a182716b3e77d3%3A0xcdfe92769e935c3d!2sAv.+Padre+C%C3%ADcero%2C+2764+-+Tri%C3%A2ngulo%2C+Juazeiro+do+Norte+-+CE!5e0!3m2!1sen!2sbr!4v1474060666408" width="100%" height="245" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h1 class="panel-title">Encontre o veículo desejado</h1>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-6">
                            <form role="form" method="post" action="<?php echo base_url("home/buscarCarros"); ?>">
                                <div class="col-sm-12">
                                    <label class="control-label col-sm-12">Marca</label>
                                    <div class="col-sm-12">
                                        <select name="marca" id="marca" class="form-control" required="">
                                            <option>Selecione</option>
                                            <?php
                                            foreach ($dados['marcas'] as $marca) {
                                                echo "<option value='" . $marca['marca'] . "'>" . $marca['marca'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="control-label col-sm-12">Modelo</label>
                                    <div class="col-sm-12">
                                        <select name="modelo" id="modelo" class="form-control">

                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <label class="control-label col-sm-12">Categoria</label>
                                    <div class="col-sm-12">
                                        <select name="categoria" id="marca" class="form-control">
                                            <?php
                                            foreach ($dados['categoria'] as $marca) {
                                                echo "<option value='" . $marca['id_categoria'] . "'>" . $marca['categoria'] . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 grupo-campos">
                                    <label class="control-label col-sm-1">Ano</label>
                                    <div class="col-sm-5">
                                        <input type="text" id="data-de" class="form-control campo-ano" name="data-de" required="" maxlength="4">
                                    </div>
                                    <label class="control-label col-sm-1">Até</label>
                                    <div class="col-sm-5">
                                        <input type="text" id="data-de" class="form-control campo-ano" name="data-ate" required="" maxlength="4">
                                    </div>
                                </div>
                                <div class="col-sm-12 grupo-campos">
                                    <label class="control-label col-sm-1">R$</label>
                                    <div class="col-sm-5">
                                        <input type="text" id="valor-de" class="form-control valor" name="valor-de" required="">
                                    </div>
                                    <label class="control-label col-sm-1">Até</label>
                                    <div class="col-sm-5">
                                        <input type="text" id="valor-ate" class="form-control valor" name="valor-ate" required="">
                                    </div>
                                </div>
                                <div class="grupo-btn-right">
                                    <button type="submit" class="btn btn-success" onclick="return validarBuscarCarro();">Bucar Veículo</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <h2 class="titulo-painel">Borges Car</h2>
                            <div id="info-empresa">
                                <ul>
                                    <li>
                                        Garantia</li>
                                    <li>
                                        Procedência</li>
                                    <li>
                                        Qualidade</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Você também pode se interessar por estes veículos</h1>
            </div>
        </div>
        <div class="row">
            <?php
            $i = 0;

            if ($tamanhoArray >= 9) {
                $i = $tamanhoArray - 9;
            }

            for ($i; $i < $tamanhoArray; $i++) {
                echo "<div class='col-md-4 img-carros' style = 'margin-top:50px;'>";
                echo "<a><img style = 'width: 360px; height: 225px' src='" . base_url . "assets/img/" . $dados['carro'][$i]['imagem'] . "' class='img-responsive'></a>"
                . "<h3>" . $dados['carro'][$i]['marca'] . "</h3>"
                . "<p>Marca: " . $dados['carro'][$i]['marca'] .
                "<br>Modelo: " . $dados['carro'][$i]['modelo'] .
                "<br>Ano: " . $dados['carro'][$i]['ano_fabricacao'] .
                "<br>Valor: R$ " . number_format($dados['carro'][$i]['valor'], 2, ',', '.') .
                "<br>Categoria: " . $dados['carro'][$i]['categoria'] .
                "<br>Combustível: " . $dados['carro'][$i]['nome_combustivel'] . "</p>"
                . "</a>"
                . "<a onclick=\"alterarIdCarro(" . '\'' . $dados['carro'][$i]['id_carro'] . '\'' . ")\" class='btn btn-success' data-toggle='modal' data-target='#mdl-deixe-proposta'>Deixe sua proposta</a>";
                echo "</div>";
            }

            /* foreach ($dados['carro'] as $carro) {
              echo "<div class='col-md-4 img-carros' style = 'margin-top:50px;'>";
              echo "<a><img style = 'width: 360px; height: 225px' src='" . base_url . "assets/img/" . $carro['imagem'] . "' class='img-responsive'></a>"
              . "<h3>" . $carro['marca'] . "</h3>"
              . "<p>Marca: " . $carro['marca'] .
              "<br>Modelo: " . $carro['modelo'] .
              "<br>Ano: " . $carro['ano_fabricacao'] .
              "<br>Valor: R$ " . number_format($carro['valor'], 2, ',', '.') .
              "<br>Categoria: " . $carro['categoria'] .
              "<br>Combustível: " . $carro['nome_combustivel'] . "</p>"
              . "</a>"
              . "<a onclick=\"alterarIdCarro(" . '\'' . $carro['id_carro'] . '\'' . ")\" class='btn btn-success' data-toggle='modal' data-target='#mdl-deixe-proposta'>Deixe sua proposta</a>";
              echo "</div>";
              } */
            ?>
        </div>
    </div>
</div>


<!-- Início popup deixar proposta-->

<div class="modal fade" id="mdl-deixe-proposta">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" contenteditable="true">Deixe sua proposta</h4>
            </div>

            <form  form-horizontal-role="form" id="mdl-deixe-proposta">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <input type="hidden" name="id-carro" id="id-carro">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputEmail3" class="control-label">Nome:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nome" placeholder="Nome completo">
                                </div>
                            </div>
                            <br> <br>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label" >Telefone:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control telefone-mask" id="telefone" placeholder="Telefone">
                                </div>
                            </div>
                            <br> <br>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label">E-mail:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="email" placeholder="E-mail">
                                </div>
                            </div>
                            <br> <br>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label" contenteditable="true">Proposta:</label>
                                </div>
                                <div class="col-sm-10">
                                    <textarea rows="3" id="proposta" cols="10" class="form-control" placeholder="Qual a melhor maneira de você pagar por este carro?"></textarea>
                                </div>
                            </div>

                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                    <button type = "reset" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Enviar Proposta</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Fim do popup deixar proposta-->


<!-- Início do popup cadastre-se-->
<div class="modal fade" id="mdl-cadastre-se">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Cadastre-se e receba nossas novidades</h4>
            </div>

            <form action="<?php echo base_url("cliente/cadastrarcliente"); ?>" method="post" class="form-horizontal" role="form">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="" class="control-label">Nome:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input name = "nome" type="text" class="form-control" placeholder="Nome Completo">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="inputPassword3" class="control-label">Telefone:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input name="telefone" type="text" class="form-control telefone-mask" placeholder="Telefone">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <label for="" class="control-label">E-mail:</label>
                                </div>
                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control" placeholder="E-mail">
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Cadastrar-se</button>
                </div>
            </form>

        </div>
    </div>
</div>
<!-- Fim do popup cadastre-se-->

<script>


    function validarBuscarCarro() {
        var modelo = document.getElementById("modelo").value;

        if (modelo !== "") {
            return true;
        } else {
            alert("Preencha todos o campos!");
            return false;
        }

        return false;



    }
</script>

