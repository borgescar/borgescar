<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Início
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Início
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-left">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Line Graph Example with Tooltips</h3>
            </div>
            <div class="panel-body">
                <div class="flot-chart">
                    <div class="flot-chart-content" id="flot-line-chart"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?php echo base_url("assets/css/bootstrap/js/jquery.js"); ?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url("assets/css/bootstrap/js/bootstrap.min.js");?>"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/morris/raphael.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/morris/morris.min.js"); ?>"></script>
<script src="<?php echo base_url("jassets/css/bootstrap/s/plugins/morris/morris-data.js"); ?>"></script>

<!-- Flot Charts JavaScript -->
<!--[if lte IE 8]><script src="js/excanvas.min.js"></script><![endif]-->
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/flot/jquery.flot.js"); ?>"></script>
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/flot/jquery.flot.tooltip.min.js"); ?>"></script>
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/flot/jquery.flot.resize.js"); ?>"></script>
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/flot/jquery.flot.pie.js"); ?>"></script>
<script src="<?php echo base_url("assets/css/bootstrap/js/plugins/flot/flot-data.js"); ?>"></script>

