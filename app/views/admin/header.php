<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Borges Car</title>
        <link href="<?php echo base_url("assets/css/bootstrap/css/bootstrap.min.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/bootstrap/css/sb-admin.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/bootstrap/css/plugins/morris.css"); ?>" rel="stylesheet">
        <link href="<?php echo base_url("assets/css/bootstrap/font-awesome/css/font-awesome.min.css"); ?>" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url; ?>assets/css/estilo.css">


        <script type="text/javascript" src="<?php echo base_url . "assets/js/funcoes.js" ?>"></script>
        <script type="text/javascript" src="<?php echo base_url . "assets/js/funcoesJquery.js" ?>"></script>
        <script src="<?php echo base_url("assets/css/bootstrap/js/jquery.js"); ?>"></script>
        <script src="<?php echo base_url("assets/css/bootstrap/js/bootstrap.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/css/bootstrap/js/plugins/morris/raphael.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/css/bootstrap/js/plugins/morris/morris.min.js"); ?>"></script>
        <script src="<?php echo base_url("assets/css/bootstrap/js/plugins/morris/morris-data.js"); ?>"></script>


        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskedinput.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskMoney.min.js') ?>"></script>

    </head>

    <body>

        <section id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url("login"); ?>" style="color: #fff">Borges Car</a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color: #fff"><i class="fa fa-user"></i> Bem-vindo(a), <?php echo $_SESSION['usuario'] ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url('admin/abaMinhaConta') ?>"><i class="fa fa-fw fa-gear"></i> Minha Conta</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo base_url . 'Login/sair' ?>"><i class="fa fa-fw fa-power-off"></i> Sair</a>
                            </li>
                        </ul>
                    </li>
                </ul>

