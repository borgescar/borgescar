<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro de Usuários
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url("admin/abaListarUsuarios");?>">Usuários Cadastrados</a>
            </li>
            <li class="active">
                <i class="fa fa-dashboard"></i> Cadastro de Usuários
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal text-right " role="form" action="<?php echo base_url('usuario/inserirUsuario') ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Nome:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteLetras(event)" type="text" class="form-control input-sm" id="nome" name="nome" placeholder="Nome">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Sexo:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="sexo" id="sexo">
                        <option value="1">Masculino</option>
                        <option value="0">Feminino</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">E-mail:</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="email" name="email" placeholder="E-mail">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Telefone:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" class="form-control input-sm" id="telefone" name="telefone" placeholder="Telefone">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">CPF:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" id="cpf" name="cpf" class="form-control input-sm" placeholder="CPF">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Usuário:</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" id="usuario" name="usuario" class="form-control input-sm" placeholder="Usuário">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Senha:</label>
                </div>
                <div class="col-sm-10">
                    <input type="password" id="senha" name="senha" class="form-control input-sm" placeholder="Senha">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return verifCadastroUsuario()" name="enviar">Cadastrar</button>
                </div>
            </div>
        </form>
    </div>
</div>