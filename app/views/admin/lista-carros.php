<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Carros Cadastrados
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Carros Cadastrados
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-primary" href="<?php echo base_url('admin/abaCadastrarCarros') ?>">Novo Carro</a>
    </div>
</div>

<br><br>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Cód.</th>
                        <th>Marca</th>
                        <th>Modelo</th>
                        <th>Ano de Fab.</th>
                        <th>Cor</th>
                        <th>Combustível</th>
                        <th>Valor(R$)</th>
                        <th>Resp. Cadastro</th>
                        <th>Vendido</th>
                        <th>Categoria</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $id;
                    $url;
                    $registro;
                    $aberturaTag;
                    foreach ($dados['listarCarros'] as $carros) {
                        $id = $carros['id_carro'];
                        $registro = "o carro de id " . $id;
                        $url = base_url('carro/excluirCarro/' . base64_encode($id) . '/' . base64_encode($carros['imagem']));
                        $aberturaTag = $id . '\',\'' . $registro . '\',\'' . $url . '\'';
                        if ($carros['fl_vendido'] == '0') {
                            $fl_vendido = 'Não';
                        } else {
                            $fl_vendido = 'Sim';
                        }
                        echo "<tr>"
                        . "<td>" . $id . "</td>"
                        . "<td>" . $carros['marca'] . "</td>"
                        . "<td>" . $carros['modelo'] . "</td>"
                        . "<td>" . $carros['ano_fabricacao'] . "</td>"
                        . "<td>" . $carros['cor'] . "</td>"
                        . "<td>" . $carros['nome_combustivel'] . "</td>"
                        . "<td>" . number_format($carros['valor'], 2, ',', '.') . "</td>"
                        . "<td>" . $carros['nome_usuario'] . "</td>"
                        . "<td>" . $fl_vendido . "</td>"
                        . "<td>" . $carros['categoria'] . "</td>"
                        . "<td>"
                        . "<a href = '" . base_url('admin/abaEditarCarros/' . $id) . "'>"
                        . "<img src='".base_url("assets/img/editar.png")."' title='Editar'>"
                        . "</a>"
                        . "</td>"
                        . "<td><a onclick=\"confirmacao(" . '\'' . $aberturaTag . ")\" href = '#'> <img src='".base_url("assets/img/excluir.png")."' title='Excluir'></a> </td>"
                        . "</tr>";
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

