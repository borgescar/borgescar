<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Envio de e-mail aos clientes
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Envio de e-mail aos clientes
            </li>
        </ol>
    </div>
</div>

<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>" type="text/javascript"></script>
<div class="row">
    <div class="col-md-12 text-left">
        <form action="<?php echo base_url("envioemail/enviaremailpromocional"); ?>" method="post" id="form-email">
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="assunto" class="control-label">Assunto</label>
                    <input type="text" name="assunto" id="assunto" class="form-control" maxlength="100" autofocus="true" placeholder="Digite aqui o assunto de seu E-mail">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="assunto" class="control-label">Mensagem</label>
                    <textarea name="email-promocional" id="email-promocional"></textarea>
                </div>
            </div>
            <script>
                CKEDITOR.replace('email-promocional');
            </script>

            <div class="row">
                <div class="col-lg-12">
                    <div class="col-sm-offset-11 col-lg-12">
                        <button type="submit" class="btn btn-primary" name="enviar">Enviar</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>



