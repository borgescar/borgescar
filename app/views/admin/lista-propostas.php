
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Propostas de Clientes
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Propostas de Clientes
            </li>
        </ol>
    </div>
</div>



<br><br>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Cód.</th>
                        <th>Data</th>
                        <th>Analisada</th>
                        <th>Cliente</th>
                        <th>Email</th>
                        <th>Telefone</th>
                        <th>Proposta</th>
                        <th>Carro</th>
                        <th>Responder</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    foreach ($dados['listarPropostas'] as $prop) {
                        $id = $prop['id_proposta'];
                        $fl_analisado = $prop['fl_analisado'];
                        if ($prop['fl_analisado'] == 1) {
                            $fl_analisado = 'Sim';
                        } else {
                            $fl_analisado = 'Não';
                        }
                        echo "<tr>"
                        . "<td>" . $id . "</td>"
                        . "<td>" . date('d/m/Y', strtotime($prop['data'])) . "</td>"
                        . "<td>" . $fl_analisado . "</td>"
                        . "<td>" . $prop['nome_pessoa'] . "</td>"
                        . "<td>" . $prop['email'] . "</td>"
                        . "<td>" . $prop['telefone'] . "</td>"
                        . "<td>" . $prop['proposta'] . "</td>"
                        . "<td style='background-color:#efefef; color:#337ab7'>". $prop['marca'] . " - ". $prop['modelo'] ." - ". $prop['ano_fabricacao'] ." - ". $prop['categoria'] ."<br>R$ ". number_format($prop['valor'], 2, ',', '.') ."</td>";
                        if ($prop['fl_analisado'] == 1) {
                            echo "<td>"
                            . "<img src = " . base_url('assets/img/responderDesativado.png') . " alt = ''>"
                            . "</td>";
                        } else {
                            echo "<td>"
                            . "<a href = '" . base_url('admin/abaResponderProposta/' . $id) . "'>"
                            . "<img src = " . base_url('assets/img/responder.png') . " alt = ''>"
                            . "</a>"
                            . "</td>"
                            . "</tr>";
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<!--Inicio modal carro de proposta-->
<div class="modal fade" id='mdl-carro-proposta'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body...</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-default" data-dismiss="modal">Close</a>
                <a class="btn btn-primary">Save changes</a>
            </div>
        </div>
    </div>
</div>
<!--Inicio modal carro de proposta-->