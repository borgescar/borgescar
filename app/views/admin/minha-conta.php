<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Minha Conta
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Alterar Senha
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form class="form-horizontal text-right" role="form" method="post" action="<?php echo base_url('usuario/alterarSenha') ?>">
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="senhaAtual" class="control-label">Senha Atual:</label>
                </div>
                <div class="col-sm-10">
                    <input type="password" class="form-control" step="1" id="senhaAtual" name="senhaAtual" placeholder="Senha Atual">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="novaSenha" class="control-label">Nova Senha:</label>
                </div>
                <div class="col-sm-10">
                    <input type="password" step="1" name="novaSenha" id="novaSenha" class="form-control" placeholder="Nova Senha">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" name="button" id="button_form" value="alterar">Alterar</button>
                </div>
            </div>
        </form>
    </div>
</div>

