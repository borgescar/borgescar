<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Início
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary" style="overflow: auto">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-long-arrow-right"></i> Quantidade de propostas no ano de <?php echo date('Y'); ?></h3>
            </div>
            <div class="panel-body">
                <div id="grafico-qtd-propostas" style="width: 100%; height: 400px; margin: 0 auto;"></div>
            </div>
        </div>
    </div>    
</div>

<script src="<?php echo base_url('assets/js/graficoLinhas.js'); ?>" type="text/javascript"></script>
<script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Mês', 'Propostas Recebidas', 'Propostas Analisadas'],
          <?php
            $meses = array('Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez');
            $qntPropostas = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            $qntPropAnalisadas = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            $anoAtual = date('Y');

            foreach ($dados['propostas'] as $p) {
                $dataProposta = explode('-', $p['data']);

                
                if ($dataProposta[0] == $anoAtual) {
                    $qntPropostas[((int) $dataProposta[1]) - 1] = $qntPropostas[((int) $dataProposta[1]) - 1] + 1;
                    if ($p['fl_analisado'] == 1) {
                        $qntPropAnalisadas[((int) $dataProposta[1]) - 1] = $qntPropAnalisadas[((int) $dataProposta[1]) - 1] + 1;
                    }
                }
            }

            for ($i = 0; $i < 12; $i++) {
                if ($i != 11) {
                    echo "['" . $meses[$i] . "'," . $qntPropostas[$i] ."," .$qntPropAnalisadas[$i] . "],";
                } else {
                    echo "['" . $meses[$i] . "'," . $qntPropostas[$i] . "," .$qntPropAnalisadas[$i] . "]";
                }
            }
            ?>
        ]);

        var options = {
          chart: {
            title: '',
            subtitle: '',
          }
        };

        var chart = new google.charts.Bar(document.getElementById('grafico-qtd-propostas'));

        chart.draw(data, options);
      }
    </script>
