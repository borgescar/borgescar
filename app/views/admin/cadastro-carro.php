<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Cadastro de Carros
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url("admin/abaListarCarros");?>">Carros Cadastrados</a>
            </li>
            <li class="active">
                <i class="fa fa-dashboard"></i> Cadastro de Carros
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-left">
        <form class="form-horizontal text-right " role="form" action="<?php echo base_url('carro/inserirCarro') ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="inputEmail3" class="control-label">Marca:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteLetras(event)" type="text" class="form-control input-sm" id="marca" name="marca" placeholder="Marca">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="inputPassword3" class="control-label">Modelo:</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="modelo" name="modelo" placeholder="Modelo">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Ano de &nbsp;Fabricação:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" class="form-control input-sm" id="anoFabricacao" name="anoFabricacao" placeholder="Ano de Fabricação">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Cor:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteLetras(event)" type="text" id="cor" name="cor" class="form-control input-sm" placeholder="Cor">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Combustivel:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="tipoCombustivel">
                        <option value="1">Álcool</option>
                        <option value="2">Gasolina</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Valor:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" id="valor" name="valor" class="form-control input-sm" placeholder="Valor">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Categoria:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="idcategoria">
                        <option value="1">Novo</option>
                        <option value="2">Usado</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="inputEmail3" class="control-label">Imagem</label>
                </div>
                <div class="col-sm-10">
                    <input type="file" name="arquivo">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success" onclick="return verificaCamposCadastroCarro()" name="enviar">Cadastrar</button>
                </div>
            </div>
        </form>
    </div>
</div>
