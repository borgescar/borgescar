<?php
foreach ($dados['carroPorId'] as $carro) {
    $idCarro = $carro['id_carro'];
    $marca = $carro['marca'];
    $modelo = $carro['modelo'];
    $ano = $carro['ano_fabricacao'];
    $cor = $carro['cor'];
    $combustivel = $carro['id_combustivel'];
    $valor = $carro['valor'];
    $id_categoria = $carro['id_categoria'];
    $vendido = $carro['fl_vendido'];
}
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Editar Carro
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url("admin/abaListarCarros"); ?>">Carros Cadastrados</a>
            </li>
            <li class="active">
                <i class="fa fa-dashboard"></i> Editar Carro
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-left">
        <form class="form-horizontal text-right " role="form" action="<?php echo base_url('carro/editarCarro/' . $idCarro) ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="inputEmail3" class="control-label">Marca:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteLetras(event)" type="text" class="form-control input-sm" id="marca" name="marca" placeholder="Marca" value="<?php echo $marca ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label for="inputPassword3" class="control-label">Modelo:</label>
                </div>

                <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" id="modelo" name="modelo" placeholder="Modelo" value="<?php echo $modelo ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Ano de &nbsp;Fabricação:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" class="form-control input-sm" id="anoFabricacao" name="anoFabricacao" placeholder="Ano de Fabricação" value="<?php echo $ano ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Cor:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteLetras(event)" type="text" id="cor" name="cor" class="form-control input-sm" placeholder="Cor" value="<?php echo $cor ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Combustivel:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="tipoCombustivel">
                        <option value="1" <?= ($combustivel == '1') ? 'selected' : '' ?> >Álcool</option>
                        <option value="2" <?= ($combustivel == '2') ? 'selected' : '' ?> >Gasolina</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Valor:</label>
                </div>
                <div class="col-sm-10">
                    <input autofocus onkeypress="return validSomenteNumeros(event)" type="text" id="valor" name="valor" class="form-control input-sm" placeholder="Valor" value="<?php echo number_format($valor, 2, ',', '.') ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Categoria:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="idcategoria">
                        <option value="1" <?= ($id_categoria == '1') ? 'selected' : '' ?> >Novo</option>
                        <option value="2" <?= ($id_categoria == '2') ? 'selected' : '' ?> >Usado</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Vendido:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="fl_vendido">
                        <option value="1" <?= ($vendido == '1') ? 'selected' : '' ?> >Sim</option>
                        <option value="0" <?= ($vendido == '0') ? 'selected' : '' ?> >Não</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" onclick="return verificaCamposCadastroCarro()" name="enviar">Alterar</button>
                </div>
            </div>
        </form>
    </div>
</div>