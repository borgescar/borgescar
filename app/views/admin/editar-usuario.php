<?php
foreach ($dados['usuarioPorId'] as $usuario) {
    $idUsuario = $usuario['id_usuario'];
    $nome = $usuario['nome_usuario'];
    $email = $usuario['email'];
    $telefone = $usuario['telefone'];
    $cpf = $usuario['cpf'];
    $login = $usuario['login'];
    $fl_ativo = $usuario['fl_ativo'];
    $fl_masculino = $usuario['fl_masculino'];
}
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Editar Usuário
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url("admin/abaListarUsuarios"); ?>">Usuários Cadastrados</a>
            </li>
            <li class="active">
                <i class="fa fa-dashboard"></i> Editar Usuário
            </li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-left">
        <form class="form-horizontal text-right " role="form" action="<?php echo base_url('usuario/editarUsuario/' . $idUsuario) ?>" enctype="multipart/form-data" method="post">
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Nome:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteLetras(event)" type="text" class="form-control input-sm" id="nome" name="nome" placeholder="Nome" value="<?php echo $nome ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Sexo:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="sexo" id="sexo">
                        <option value="1" <?= ($fl_masculino == '1') ? 'selected' : '' ?> >Masculino</option>
                        <option value="0" <?= ($fl_masculino == '0') ? 'selected' : '' ?> >Feminino</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">E-mail:</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" class="form-control input-sm" onclick="return verifCadastroUsuario()" id="email" name="email" placeholder="E-mail" value="<?php echo $email ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Telefone:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" class="form-control input-sm" id="telefone" name="telefone" placeholder="Telefone" value="<?php echo $telefone ?>">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">CPF:</label>
                </div>
                <div class="col-sm-10">
                    <input onkeypress="return validSomenteNumeros(event)" type="text" id="cpf" name="cpf" class="form-control input-sm" placeholder="CPF" value="<?php echo $cpf ?>" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Usuário:</label>
                </div>
                <div class="col-sm-10">
                    <input type="text" id="usuario" name="usuario" class="form-control input-sm" placeholder="Usuário" value="<?php echo $login ?>" disabled>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2">
                    <label class="control-label">Ativo:</label>
                </div>
                <div class="col-sm-10">
                    <select class="form-control" name="fl_ativo" id="fl_ativo">
                        <option value="1" <?= ($fl_ativo == '1') ? 'selected' : '' ?> >Sim</option>
                        <option value="0" <?= ($fl_ativo == '0') ? 'selected' : '' ?> >Não</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" onclick="return verifCadastroUsuario()" name="enviar">Editar</button>
                </div>
            </div>
        </form>
    </div>
</div>