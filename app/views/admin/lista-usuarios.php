<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Usuários Cadastrados
        </h1>
        <ol class="breadcrumb">
            <li class="active">
                <i class="fa fa-dashboard"></i> Usuários Cadastrados
            </li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <a class="btn btn-primary" href="<?php echo base_url('admin/abaCadastrarUsuarios') ?>">Novo Usuário</a>
    </div>
</div>


<br><br>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Cód.</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>CPF</th>
                        <th>Login</th>
                        <th>Ativo</th>
                        <th>Sexo</th>
                        <th>Administrador</th>
                        <th>Editar</th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                    $codigo = 1;
                    foreach ($dados['listarUsuarios'] as $usuarios) {
                        $id = $usuarios['id_usuario'];
                        if ($usuarios['fl_ativo'] == '1') {
                            $fl_ativo = 'Sim';
                        } else {
                            $fl_ativo = 'Não';
                        }
                        if ($usuarios['fl_masculino'] == '1') {
                            $fl_masculino = 'Masculino';
                        } else {
                            $fl_masculino = 'Feminino';
                        }
                        if ($usuarios['fl_admin'] == '1') {
                            $fl_admin = 'Sim';
                        } else {
                            $fl_admin = 'Não';
                        }
                        echo "<tr>"
                        . "<td>" . $codigo . "</td>"
                        . "<td>" . $usuarios['nome_usuario'] . "</td>"
                        . "<td>" . $usuarios['email'] . "</td>"
                        . "<td>" . $usuarios['telefone'] . "</td>"
                        . "<td>" . $usuarios['cpf'] . "</td>"
                        . "<td>" . $usuarios['login'] . "</td>"
                        . "<td>" . $fl_ativo . "</td>"
                        . "<td>" . $fl_masculino . "</td>"
                        . "<td>" . $fl_admin . "</td>"
                        . "<td>"
                        . "<a href = '" . base_url('admin/abaEditarUsuarios/' . $id) . "'>"
                        . "<img src = " . base_url('assets/img/editar.png') . " alt = ''>"
                        . "</a>"
                        . "</td>"
                        . "</tr>";

                        $codigo++;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>