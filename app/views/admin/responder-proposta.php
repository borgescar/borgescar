<?php
foreach ($dados['dadosProposta'] as $prop) {
    $idProposta = $prop['id_proposta'];
    $email = $prop['email'];
    $nome = $prop['nome_pessoa'];
}
?>

<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            Responder Proposta de Cliente
        </h1>
        <ol class="breadcrumb">
            <li>
                <i class="fa fa-dashboard"></i>  <a href="<?php echo base_url("admin/abaListarPropostas"); ?>">Propostas de Cliente</a>
            </li>
            <li class="active">
                <i class="fa fa-dashboard"></i> Responder Proposta de Cliente
            </li>
        </ol>
    </div>
</div>

<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>" type="text/javascript"></script>
<div class="row">
    <div class="col-md-12 text-left">
        <label class="control-label">Resposta</label>
        <form class="form-horizontal text-right " role="form" action="<?php echo base_url('PropostaCont/inserirResposta'); ?>" method="post">

            <div>
                <input type="hidden" name="id-proposta" id="id-proposta" value="<?php echo $idProposta ?>">
                <input type="hidden" name="email-proposta" id="email-proposta" value="<?php echo $email ?>">
                <input type="hidden" name="nome-proposta" id="email-proposta" value="<?php echo $nome ?>">
            </div>

            <div class="col-lg-12">   
                <div class="form-group">
                    <textarea rows="3" id="resposta" name="resposta" cols="10" class="form-control" ></textarea>

                    <script>
                        CKEDITOR.replace('resposta');
                    </script>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary" name="enviar">Enviar Resposta</button>
                </div>
            </div>
        </form>
    </div> 
</div>


