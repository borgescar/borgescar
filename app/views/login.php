<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url; ?>assets/css/estilo.css">
        <script type="text/javascript" src="<?php echo base_url . "assets/js/funcoes.js" ?>"></script>
    </head>

    <body data-spy="scroll" id="fundo-login">

        <div class="container-fluid">
            <div id ="alert-login" class="row alert-login">
                
            </div>


            <section id="login">

                <div id="img-login">
                    <div>
                        <i class="fa fa-user"></i>
                    </div>
                </div>

                <div id="form-login">

                    <form class="form-horizontal" action="<?php echo base_url . "login/logar"; ?>" method="post" role="form">

                        <div class="form-group">
                            <label class="control-label">Usuário</label>
                            <input type="text" class="form-control" placeholder="Usuário" name="usuario" id="usuario">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Senha</label>
                            <input type="password" class="form-control" id="senha" placeholder="Senha" name="senha">
                        </div>

                        <div class="row botao-login">
                            <button type="submit" class="btn btn-primary" onclick="return verificaCamposLogin()">
                                <i class="fa fa-fw fa-lock"></i>Entrar
                            </button>
                        </div>

                        <div id="recuperar-senha">
                            <a href="<?php echo base_url('login/abaRecuperarSenha') ?>">Esqueci a senha</a>
                        </div>  


                    </form>

                </div> 

            </section>
        </div>

    </body>

</html>
