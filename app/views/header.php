<!DOCTYPE html>

<html lang="pt-BR">

    <head>
        <title>Borges Car</title>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script type="text/javascript" src="<?php echo base_url("assets/pingendo/jquery.min.js");?>"></script>
        <script type="text/javascript" src="<?php echo base_url("assets/pingendo/boostrap.min.js");?>"></script>

        <script type="text/javascript" src="<?php echo base_url."assets/js/funcoes.js"?>"></script>
        <script type="text/javascript" src="<?php echo base_url."assets/js/funcoesJquery.js"?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskedinput.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.maskMoney.min.js') ?>"></script>
        
        <link href="<?php echo base_url("assets/pingendo/fonte-awesome.min.css"); ?>" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url;?>assets/css/estilo.css">
        <link href="<?php echo base_url("assets/pingendo/bootstrap.css"); ?>" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <script type="text/javascript">
            baseUrl = "<?php echo base_url();?>";
        </script>
        
        <div class="navbar navbar-default navbar-fixed-top" id="menu-cabecalho">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" title="Borges Car"><img height="20" alt="Brand" src="<?php echo base_url;?>assets/img/logo-cabecalho.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="navbar-ex-collapse">
                    <ul class="nav navbar-nav navbar-right itens-menu">
                        <a href="#" data-toggle="modal" data-target="#mdl-cadastre-se">
                            <button class="btn-primary">
                                Cadastre-se
                            </button>
                        </a>
                    </ul>
                </div>
            </div>
        </div>

