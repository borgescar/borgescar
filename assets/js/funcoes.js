function alterarIdCarro(idCarro) {
    document.getElementById("id-carro").value = idCarro;
}

function verificaCamposLogin() {
    var usuario = document.getElementById("usuario").value;
    var senha = document.getElementById("senha").value;
    if (usuario == '' || senha == '') {
        
        html = "<div class=\"alert alert-danger\"><button class=\"close\" data-dismiss=\"alert\">"+
                        "<span>"+
                            "&times;"+
                       " </span>"+
                    "</button>"+
                    "<strong>Atenção</strong>, informe o usuário e senha!"+
                "</div>";
        
        
        document.getElementById("alert-login").innerHTML = html;
        return false;
    } else {
        return true;
    }
}

function fecharAlert(){
    document.getElementById("alert-login").style.display = "none";
}

function verificaCamposEsqueciSenha() {
    var emailRec = document.getElementById("emailRec").value;
    var usuarioRec = document.getElementById("usuarioRec").value;
    if (emailRec == '' || usuarioRec == '') {
          html = "<div class=\"alert alert-danger\"><button class=\"close\" data-dismiss=\"alert\">"+
                        "<span>"+
                            "&times;"+
                       " </span>"+
                    "</button>"+
                    "<strong>Atenção</strong>, informe seu e-mail e usuário!"+
                "</div>";
        
        
        document.getElementById("alert-login").innerHTML = html;
        return false;
    } else {
        return true;
    }
}

function verificaCamposCadastroCarro() {
    var marca = document.getElementById("marca").value;
    var modelo = document.getElementById("modelo").value;
    var anoFabricacao = document.getElementById("anoFabricacao").value;
    var cor = document.getElementById("cor").value;
    var valor = document.getElementById("valor").value;

    if (marca == '' || modelo == '' || anoFabricacao == '' || cor == '' || valor == '') {
        alert('Atenção, nenhum dos campos deve ficar em branco!');
        return false;
    } else {
        return true;
    }
}

function verifCadastroUsuario() {
    var nome = document.getElementById("nome").value;
    var sexo = document.getElementById("sexo").value;
    var email = document.getElementById("email").value;
    var telefone = document.getElementById("telefone").value;
    var cpf = document.getElementById("cpf").value;
    var usuario = document.getElementById("usuario").value;
    var senha = document.getElementById("senha").value;
    if (nome == '' || sexo == '' || email == '' || telefone == '' || cpf == '' || usuario == '' || senha == '') {
        alert('Atenção, nenhum dos campos deve ficar em branco!');
        return false;
    } else {
        return true;
    }
}

function validSomenteLetras(e) {
    var expressao;
    expressao = /[a-zA-Z _-]/;

    if (expressao.test(String.fromCharCode(e.keyCode))) {
        return true;
    }
    else {
        return false;
    }
}

function validSomenteNumeros(e) {
    var expressao;
    expressao = /[ 0-9_-]/;

    if (expressao.test(String.fromCharCode(e.keyCode))) {
        return true;
    }
    else {
        return false;
    }
}

function confirmacao(id, registro, url) {
    var resposta = confirm("Você removerá " + registro + "!");

    if (resposta == true) {
        window.location.href = url;
    }
}



