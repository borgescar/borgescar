var baseUrl;

$(document).ready(function () {
    $("#mdl-deixe-proposta").submit(function (event) {


        var dados = {
            carro: document.getElementById("id-carro").value,
            nome: document.getElementById("nome").value,
            telefone: document.getElementById("telefone").value,
            email: document.getElementById("email").value,
            proposta: document.getElementById("proposta").value
        };

        if (document.getElementById("nome").value != "" ||
                document.getElementById("telefone").value != "" ||
                document.getElementById("email").value != "" ||
                document.getElementById("proposta").value != ""
                ) {
            $.post(baseUrl + "propostacont/inserirProposta", dados, function (data) {
                alert(data.msg);
                $('#mdl-deixe-proposta').each(function(){
                  this.reset();
                });
            }, 'json');
        } else {
            alert("Preencha todos os campos!");
        }

        event.preventDefault(); //permite que a página não seja recarregada
    });
    
    $("#marca").change(function (){
        var marca = $("#marca").val();
        var html = "";
        
        if(marca != ""){
            $.post(baseUrl+"home/buscarModelos", {marc:marca}, function(data){
                
                for(var i in data){
                    html += "<option value=\""+data[i].modelo+"\">"+data[i].modelo+"</option>";
                }
                document.getElementById("modelo").innerHTML = html;
            },'json');
        }
    });
});


